class AddExtraFieldToNotes < ActiveRecord::Migration[6.0]
  def change
    add_column :notes, :state, :integer, null: false, default: 0
    add_column :notes, :due_date, :datetime
    add_column :notes, :completed_date, :datetime
    add_reference :notes, :tag, foreign_key: true, type: :uuid, index: true
  end
end
