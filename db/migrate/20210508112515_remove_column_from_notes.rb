class RemoveColumnFromNotes < ActiveRecord::Migration[6.0]
  def up
    remove_column :notes, :state, :integer
    add_column :notes, :state, :string, index: true
  end

  def down
    add_column :notes, :state, :integer
    remove_column :notes, :state, :string, index: true
  end
end
