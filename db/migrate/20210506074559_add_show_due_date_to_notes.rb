class AddShowDueDateToNotes < ActiveRecord::Migration[6.0]
  def change
    add_column :notes, :show_due_date, :boolean, default: false, null: false
  end
end
