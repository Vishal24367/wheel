# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    trait :issue do
      name { 'Issue' }
      color { 'yellow' }
      user
    end

    trait :bug do
      name { 'Bug' }
      description { 'red' }
      user
    end

    trait :change do
      name { 'Change' }
      description { 'blue' }
      user
    end
  end
end
