# frozen_string_literal: true

require 'test_helper'

class TagTest < ActiveSupport::TestCase
  def setup
    @admin = build(:user, :admin)
  end

  def test_valid_tag
    tag = build(:tag, @admin)
    puts tag
    assert tag.valid?
  end

  def test_invalid_tag
    invalid_tag = { name: "",
                        color: "",
                        user: @admin
                      }

    tag = tag.new(invalid_tag)

    assert_not tag.valid?
    assert_includes tag.errors.full_messages, "Name can't be blank"
    assert_includes tag.errors.full_messages, "Color can't be blank"
  end
end
