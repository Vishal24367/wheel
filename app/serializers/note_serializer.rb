# frozen_string_literal: true

class NoteSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :due_date, :completed_date, :state, :created_at, :tag_name, :tag_color, :show_due_date

  def created_at
    self.object.created_at.strftime("%b%e, %Y") rescue "--"
  end

  def state
    self.object.state.split('_').map(&:capitalize).join(' ') rescue "--"
  end

  def due_date
    self.object.show_due_date ? self.object.due_date.strftime("%b%e, %Y") : "--"
  end

  def completed_date
    self.object.try(:completed_date).strftime("%b%e, %Y") rescue "--"
  end

  def tag
    self.object.try(:tag) rescue nil
  end

  def tag_name
    tag.try(:name) rescue "--"
  end

  def tag_color
    tag.try(:color) rescue "grey"
  end
end
