import React from "react";
// @ts-ignore
import { Tooltip, Button } from "neetoui";

export default function TooltipBadge({
  content,
  icon,
  updateTaskId,
  updateTaskState,
  taskId,
  taskState,
}) {
  return (
    <>
      <Tooltip content={content} position="bottom" className="ml-4">
        <Button
          style="icon"
          icon={icon}
          onClick={() => {
            if (taskState === "delete") {
              updateTaskId(taskId);
              updateTaskState();
            } else {
              updateTaskId(taskId);
              updateTaskState(taskId, taskState);
            }
          }}
        />
      </Tooltip>
    </>
  );
}
