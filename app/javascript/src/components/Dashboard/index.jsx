import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";

import Navbar from "components/Common/Navbar";

import PasswordEdit from "./Account/Passwords/Edit";
// import Profile from "./Account/Profile";
import Tasks from "./Tasks";
import Tags from "./Tags";
import { TaskProvider } from "contexts/task";
import { TagProvider } from "contexts/tag";

const Home = () => {
  return (
    <div className="flex h-screen">
      <Navbar />
      <div className="flex flex-col items-start justify-start flex-grow h-screen overflow-y-auto">
        <Switch>
          <TagProvider>
            <TaskProvider>
              <Route exact path="/tasks" component={Tasks} />
              <Route exact path="/tags" component={Tags} />
              <Route exact path="/my/password/edit" component={PasswordEdit} />
              <Redirect from="/" to="/tasks" />
            </TaskProvider>
          </TagProvider>
        </Switch>
      </div>
    </div>
  );
};

export default Home;
