import React, { useState } from "react";
// @ts-ignore
import { Alert } from "neetoui";
import tasksApi from "apis/tasks";

export default function DeleteAlert({
  onClose,
  selectedTaskIds,
  selectedTaskId,
  updateTaskState,
}) {
  const [deleting, setDeleting] = useState(false);
  const handleDelete = async () => {
    try {
      setDeleting(true);
      if (selectedTaskIds.length > 0) {
        await tasksApi.destroy({ ids: selectedTaskIds });
      } else {
        await tasksApi.deleteTask(selectedTaskId);
        updateTaskState(selectedTaskId, "DELETE_TASK");
      }
      onClose();
    } catch (error) {
      // @ts-ignore
      logger.error(error);
    } finally {
      setDeleting(false);
    }
  };
  return (
    <Alert
      isOpen
      size="small"
      autoHeight
      title={`Delete ${
        selectedTaskIds.length > 0 ? selectedTaskIds.length : 1
      } ${selectedTaskIds.length > 1 ? "Tasks" : "Task"}`}
      submitButtonProps={{
        style: "danger",
        label: "Delete",
        loading: deleting,
        onClick: handleDelete,
      }}
      cancelButtonProps={{
        onClick: onClose,
      }}
      onClose={onClose}
    ></Alert>
  );
}
