import React, { useState, useEffect } from "react";
import tasksApi from "apis/tasks";
// @ts-ignore
import { Button, PageLoader } from "neetoui";
import EmptyState from "components/Common/EmptyState";
// @ts-ignore
import EmptyNotesListImage from "images/EmptyNotesList";
// @ts-ignore
import { Header, SubHeader } from "neetoui/layouts";

import TaskTable from "./TaskTable";

import DeleteAlert from "./DeleteAlert";
import NewTaskPane from "./NewTaskPane";
import { useTaskDispatch, useTaskState } from "contexts/task";
import Logger from "js-logger";

const Tasks = () => {
  const [loading, setLoading] = useState(true);
  const [showNewTaskPane, setShowNewTaskPane] = useState(false);
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedTaskIds, setselectedTaskIds] = useState([]);
  const [selectedTaskId, setselectedTaskId] = useState([]);
  const taskDispatch = useTaskDispatch();
  const { tasks } = useTaskState();

  useEffect(() => {
    fetchTasks();
  }, []);

  const updateTaskState = (task, actionType) => {
    if (actionType !== "DELETE_TASK")
      taskDispatch({
        tasks,
        type: actionType,
        payload: {
          id: task.id,
          title: task.title,
          state: task.state,
          tag_name: task.tag_name,
          tag_color: task.tag_color,
          due_date: task.due_date,
          created_at: task.created_at,
          completed_date: task.completed_date,
          description: task.description,
          show_due_date: task.show_due_date,
        },
      });
    else
      taskDispatch({
        tasks,
        type: actionType,
        payload: {
          id: task,
        },
      });
  };

  const fetchTasks = async () => {
    try {
      setLoading(true);
      const response = await tasksApi.fetch();
      response.data.data.notes.map(task => updateTaskState(task, "ADD_TASK"));
    } catch (error) {
      // @ts-ignore
      logger.error(error);
    } finally {
      setLoading(false);
    }
  };

  const updateTaskAction = async (id, taskState) => {
    try {
      const response = await tasksApi.moveTask(id, {
        state: taskState,
      });
      updateTaskState(response.data.data.note, "UPDATE_TASK");
    } catch (error) {
      Logger.error(error);
    }
  };

  const deleteTaskAction = async () => {
    setShowDeleteAlert(true);
  };

  if (loading) {
    return <PageLoader />;
  }
  return (
    <>
      <Header
        title="Notes"
        actionBlock={
          <Button
            onClick={() => setShowNewTaskPane(true)}
            label="Add New Task"
            icon="ri-add-line"
          />
        }
      />
      {tasks.length ? (
        <>
          <SubHeader
            searchProps={{
              value: searchTerm,
              onChange: e => setSearchTerm(e.target.value),
              clear: () => setSearchTerm(""),
            }}
            deleteButtonProps={{
              onClick: () => setShowDeleteAlert(true),
              disabled: !selectedTaskIds.length,
            }}
            paginationProps={{
              count: tasks.length,
              pageNo: 1,
              pageSize: 15,
            }}
            toggleFilter={{
              value: "note",
            }}
            sortProps={{
              options: [
                { label: "Title", value: "title" },
                { label: "State", value: "state" },
                { label: "Tag", value: "tag_name" },
                { label: "Due Date", value: "due_date" },
              ],
            }}
          />
          <TaskTable
            selectedTaskIds={selectedTaskIds}
            setSelectedTaskIds={setselectedTaskIds}
            setselectedTaskId={setselectedTaskId}
            tasks={tasks}
            deleteTaskAction={deleteTaskAction}
            updateTaskAction={updateTaskAction}
          />
        </>
      ) : (
        <EmptyState
          image={EmptyNotesListImage}
          title="Looks like you don't have any notes!"
          subtitle="Add your notes to send customized emails to them."
          primaryAction={() => setShowNewTaskPane(true)}
          primaryActionLabel="Add New Task"
        />
      )}
      <NewTaskPane
        showPane={showNewTaskPane}
        setShowPane={setShowNewTaskPane}
        updateTaskState={updateTaskState}
      />
      {showDeleteAlert && (
        <DeleteAlert
          selectedTaskIds={selectedTaskIds}
          selectedTaskId={selectedTaskId}
          onClose={() => setShowDeleteAlert(false)}
          updateTaskState={updateTaskState}
        />
      )}
    </>
  );
};

export default Tasks;
