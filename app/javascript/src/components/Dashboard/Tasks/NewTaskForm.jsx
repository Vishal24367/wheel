import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { Formik, Form } from "formik";
// @ts-ignore
import { Input, Textarea } from "neetoui/formik";
// @ts-ignore
import { Button, Select, Switch, DateInput } from "neetoui";
import tasksApi from "apis/tasks";
import tagsApi from "apis/tags";
import Logger from "js-logger";

export default function NewTaskForm({ onClose, updateTaskState }) {
  // @ts-ignore
  const [loading, setLoading] = useState(true);
  const [tagsOption, setTagsOption] = useState([]);

  useEffect(() => {
    fetchTags();
  }, []);

  const fetchTags = async () => {
    try {
      setLoading(true);
      const response = await tagsApi.fetch();
      response.data.data.tags.map(tag => {
        let data = { value: tag.id, label: tag.name };
        setTagsOption(tagsOption => [...tagsOption, data]);
      });
    } catch (error) {
      // @ts-ignore
      Logger.error(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    !loading &&
    tagsOption.length && (
      <Formik
        initialValues={{
          title: "",
          description: "",
          tag: tagsOption[0],
          showDueDateField: false,
          dueDate: new Date(),
        }}
        onSubmit={async values => {
          try {
            let note = { note: values };
            const response = await tasksApi.create(note);
            updateTaskState(response.data.data.note, "ADD_TASK");
            onClose();
          } catch (err) {
            // @ts-ignore
            Logger.error(err);
          }
        }}
        validationSchema={yup.object({
          title: yup.string().required("Title is required"),
          description: yup.string().required("Description is required"),
          showDueDateField: yup.boolean(),
          dueDate: yup.date().when("showDueDateField", {
            is: true,
            then: yup.date().required("Date is required").nullable(),
            otherwise: yup.date().nullable(),
          }),
        })}
      >
        {({ isSubmitting, setFieldValue, values }) => (
          <Form>
            <Input label="Title" name="title" className="mb-6" />
            <Textarea
              label="Description"
              className="mb-6"
              name="description"
              rows={8}
            />
            <Select
              className="mb-6"
              label="Tag"
              defaultValue={tagsOption[0]}
              placeholder="Select an Tag"
              isDisabled={false}
              isClearable={true}
              isSearchable={true}
              name="tag"
              value={values.tag}
              onChange={value => setFieldValue("tag", value)}
              options={tagsOption}
            />
            <div className="flex justify-between items-center mb-2">
              <span>Add Due Date to Note</span>
              <Switch
                name="showDueDateField"
                checked={values.showDueDateField}
                onChange={() =>
                  setFieldValue("showDueDateField", !values.showDueDateField)
                }
              />
            </div>
            {values.showDueDateField && (
              <DateInput
                name={"dueDate"}
                value={values.dueDate}
                onChange={value => setFieldValue("dueDate", value)}
                label="Due Date"
                minDate={new Date()}
              />
            )}
            <div className="nui-pane__footer nui-pane__footer--absolute">
              <Button
                onClick={onClose}
                label="Cancel"
                size="large"
                style="secondary"
              />

              <Button
                type="submit"
                label="Submit"
                size="large"
                style="primary"
                className="ml-2"
                disabled={isSubmitting}
                loading={isSubmitting}
              />
            </div>
          </Form>
        )}
      </Formik>
    )
  );
}
