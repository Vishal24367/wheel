import React from "react";
// @ts-ignore
import { Pane } from "neetoui";
import NewTaskForm from "./NewTaskForm";

export default function NewTaskPane({
  updateTaskState,
  showPane,
  setShowPane,
}) {
  const onClose = () => setShowPane(false);
  return (
    <Pane title="Create a New Note" isOpen={showPane} onClose={onClose}>
      <div className="px-6">
        <NewTaskForm onClose={onClose} updateTaskState={updateTaskState} />
      </div>
    </Pane>
  );
}
