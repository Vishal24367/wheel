/* eslint-disable indent */
import React from "react";
// @ts-ignore
import { Checkbox, Badge, Tooltip, Button } from "neetoui";
import TooltipBadge from "components/Common/TooltipBadge";

export default function TaskTable({
  selectedTaskIds,
  setSelectedTaskIds,
  deleteTaskAction,
  setselectedTaskId,
  updateTaskAction,
  tasks = [],
}) {
  return (
    <div className="w-full px-4">
      <table className="nui-table nui-table--checkbox">
        <thead>
          <tr>
            <th>
              <Checkbox
                checked={
                  selectedTaskIds.length === tasks.map(task => task.id).length
                }
                onClick={() => {
                  const taskIds = tasks.map(note => note.id);
                  if (selectedTaskIds.length === taskIds.length) {
                    setSelectedTaskIds([]);
                  } else {
                    setSelectedTaskIds(taskIds);
                  }
                }}
              />
            </th>
            <th className="text-left">Title</th>
            <th className="text-left">Description</th>
            <th className="text-center">Tags</th>
            <th className="text-center">Status</th>
            <th className="text-center">Created On</th>
            <th className="text-center">Due Date</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {tasks.map(task => (
            <tr
              key={task.id}
              className={"cursor-pointer bg-white hover:bg-gray-50"}
            >
              <td>
                <Checkbox
                  checked={selectedTaskIds.includes(task.id)}
                  onClick={event => {
                    event.stopPropagation();
                    const index = selectedTaskIds.indexOf(task.id);

                    if (index > -1) {
                      setSelectedTaskIds([
                        ...selectedTaskIds.slice(0, index),
                        ...selectedTaskIds.slice(index + 1),
                      ]);
                    } else {
                      setSelectedTaskIds([...selectedTaskIds, task.id]);
                    }
                  }}
                />
              </td>
              <td>
                <div className="flex flex-row items-center justify-start text-gray-900">
                  <Button
                    type="link"
                    label={task.title}
                    style="text"
                    className="text-blue-600"
                  />
                </div>
              </td>
              <td className="text-center">
                <div className="flex flex-row justify-start">
                  <Tooltip content={task.description} position="bottom">
                    {task.description}
                  </Tooltip>
                </div>
              </td>
              <td className="text-center">
                <Badge size="large" color={task.tag_color}>
                  {task.tag_name}
                </Badge>
              </td>
              <td className="text-center">
                <Badge
                  size="large"
                  color={
                    task.state === "Newly Created"
                      ? "blue"
                      : task.state === "In Progress"
                      ? "purple"
                      : task.state === "On Hold"
                      ? "yellow"
                      : task.state === "Completed"
                      ? "green"
                      : "red"
                  }
                >
                  {task.state}
                </Badge>
              </td>
              <td className="text-center">{task.created_at}</td>
              <td className="text-center">{task.due_date}</td>
              <td className="text-center">
                <div className="flex flex-row items-center justify-center">
                  {task.state === "Newly Created" && (
                    <TooltipBadge
                      content={"Edit"}
                      icon={"ri-pencil-fill"}
                      taskId={task.id}
                      taskState={"edit"}
                      updateTaskId={setselectedTaskId}
                      updateTaskState={updateTaskAction}
                    />
                  )}
                  <TooltipBadge
                    content={"Delete"}
                    icon={"ri-delete-bin-line"}
                    taskId={task.id}
                    taskState={"delete"}
                    updateTaskId={setselectedTaskId}
                    updateTaskState={deleteTaskAction}
                  />
                  {task.state === "Newly Created" ? (
                    <>
                      <TooltipBadge
                        content={"In Progress"}
                        icon={"ri-play-line"}
                        taskId={task.id}
                        taskState={"in_progress"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                      <TooltipBadge
                        content={"Close"}
                        icon={"ri-close-circle-line"}
                        taskId={task.id}
                        taskState={"closed"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                    </>
                  ) : task.state === "In Progress" ? (
                    <>
                      <TooltipBadge
                        content={"On Hold"}
                        icon={"ri-rest-time-fill"}
                        taskId={task.id}
                        taskState={"on_hold"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                      <TooltipBadge
                        content={"Completed"}
                        icon={"ri-check-double-line"}
                        taskId={task.id}
                        taskState={"completed"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                      <TooltipBadge
                        content={"Close"}
                        icon={"ri-close-circle-line"}
                        taskId={task.id}
                        taskState={"closed"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                    </>
                  ) : task.state === "On Hold" ? (
                    <>
                      <TooltipBadge
                        content={"In Progress"}
                        icon={"ri-play-line"}
                        taskId={task.id}
                        taskState={"in_progress"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                      <TooltipBadge
                        content={"Close"}
                        icon={"ri-close-circle-line"}
                        taskId={task.id}
                        taskState={"closed"}
                        updateTaskId={setselectedTaskId}
                        updateTaskState={updateTaskAction}
                      />
                    </>
                  ) : null}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
