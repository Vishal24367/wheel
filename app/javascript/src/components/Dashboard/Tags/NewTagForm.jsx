import React from "react";
import * as yup from "yup";
import { Formik, Form } from "formik";
// @ts-ignore
import { Input } from "neetoui/formik";
// @ts-ignore
import { Button, Select } from "neetoui";
import tagsApi from "apis/tags";

export default function NewTagForm({ onClose, updateTagState }) {
  return (
    <Formik
      initialValues={{
        name: "",
        color: { value: "blue", label: "Blue" },
      }}
      onSubmit={async values => {
        try {
          const response = await tagsApi.create(values);
          updateTagState(response.data.data.tag, "ADD_TAG");
          onClose();
        } catch (err) {
          // @ts-ignore
          logger.error(err);
        }
      }}
      validationSchema={yup.object({
        name: yup.string().required("Name is required"),
        color: yup.object().required("Color is required"),
      })}
    >
      {({ isSubmitting, setFieldValue, values }) => (
        <Form>
          <Input label="Name" name="name" className="mb-6" />
          <Select
            label="Select Color"
            defaultValue={{ value: "blue", label: "Blue" }}
            placeholder="Select an Option"
            isDisabled={false}
            isClearable={true}
            isSearchable={true}
            name="color"
            value={values.color}
            onChange={value => setFieldValue("color", value)}
            options={[
              { value: "red", label: "Red" },
              { value: "yellow", label: "Yellow" },
              { value: "green", label: "Green" },
              { value: "blue", label: "Blue" },
              { value: "grey", label: "Grey" },
            ]}
          />
          <div className="nui-pane__footer nui-pane__footer--absolute">
            <Button
              onClick={onClose}
              label="Cancel"
              size="large"
              style="secondary"
            />

            <Button
              type="submit"
              label="Submit"
              size="large"
              style="primary"
              className="ml-2"
              disabled={isSubmitting}
              loading={isSubmitting}
            />
          </div>
        </Form>
      )}
    </Formik>
  );
}
