import React from "react";
// @ts-ignore
import { Checkbox, Badge, Tooltip, Button } from "neetoui";

export default function TagTable({
  selectedTagIds,
  setSelectedTagIds,
  deleteTagAction,
  tags = [],
}) {
  return (
    <div className="w-full px-4">
      <table className="nui-table nui-table--checkbox">
        <thead>
          <tr>
            <th>
              <Checkbox
                checked={
                  selectedTagIds.length === tags.map(tag => tag.id).length
                }
                onClick={() => {
                  const tagIds = tags.map(tag => tag.id);
                  if (selectedTagIds.length === tagIds.length) {
                    setSelectedTagIds([]);
                  } else {
                    setSelectedTagIds(tagIds);
                  }
                }}
              />
            </th>
            <th className="text-left">Name</th>
            <th className="text-center">Color</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {tags.map(tag => (
            <tr
              key={tag.id}
              className={"cursor-pointer bg-white hover:bg-gray-50"}
            >
              <td>
                <Checkbox
                  checked={selectedTagIds.includes(tag.id)}
                  onClick={event => {
                    event.stopPropagation();
                    const index = selectedTagIds.indexOf(tag.id);

                    if (index > -1) {
                      setSelectedTagIds([
                        ...selectedTagIds.slice(0, index),
                        ...selectedTagIds.slice(index + 1),
                      ]);
                    } else {
                      setSelectedTagIds([...selectedTagIds, tag.id]);
                    }
                  }}
                />
              </td>
              <td>
                <div className="flex flex-row items-center justify-start text-gray-900">
                  {tag.name}
                </div>
              </td>
              <td className="text-center">
                <Badge size="large" color={tag.color}>
                  {tag.color}
                </Badge>
              </td>
              <td className="text-center">
                <div className="flex flex-row items-center justify-center">
                  <Tooltip content={"Edit"} position="bottom">
                    <Button style="icon" icon="ri-pencil-fill" />
                  </Tooltip>
                  <Tooltip
                    content={"Delete"}
                    position="bottom"
                    className="ml-4"
                  >
                    <Button
                      style="icon"
                      icon="ri-delete-bin-line"
                      onClick={() => deleteTagAction(tag.id)}
                    />
                  </Tooltip>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
