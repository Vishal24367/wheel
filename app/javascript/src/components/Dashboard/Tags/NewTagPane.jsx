import React from "react";
// @ts-ignore
import { Pane } from "neetoui";
import NewTagForm from "./NewTagForm";

export default function NewTagPane({ showPane, setShowPane, updateTagState }) {
  const onClose = () => setShowPane(false);
  return (
    <Pane title="Create a New Tag" isOpen={showPane} onClose={onClose}>
      <div className="px-6">
        <NewTagForm onClose={onClose} updateTagState={updateTagState} />
      </div>
    </Pane>
  );
}
