import React, { useState, useEffect } from "react";
import tagsApi from "apis/tags";
// @ts-ignore
import { Button, PageLoader } from "neetoui";
import EmptyState from "components/Common/EmptyState";
// @ts-ignore
import EmptyNotesListImage from "images/EmptyNotesList";
// @ts-ignore
import { Header, SubHeader } from "neetoui/layouts";

import TagTable from "./TagTable";
import NewTagPane from "./NewTagPane";
import DeleteAlert from "./DeleteAlert";
import { useTagDispatch, useTagState } from "contexts/tag";

const Tags = () => {
  const [loading, setLoading] = useState(true);
  const [showNewTagPane, setShowNewTagPane] = useState(false);
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedTagIds, setSelectedTagIds] = useState([]);
  const [selectedTagId, setselectedTagId] = useState([]);
  const tagDispatch = useTagDispatch();
  const { tags } = useTagState();

  useEffect(() => {
    fetchTags();
  }, []);

  const updateTagState = (tag, actionType) => {
    if (actionType !== "DELETE_TAG")
      tagDispatch({
        tags,
        type: actionType,
        payload: {
          id: tag.id,
          color: tag.color,
          name: tag.name,
        },
      });
    else
      tagDispatch({
        tags,
        type: actionType,
        payload: {
          id: tag,
        },
      });
  };

  const fetchTags = async () => {
    try {
      setLoading(true);
      const response = await tagsApi.fetch();
      response.data.data.tags.map(tag => updateTagState(tag, "ADD_TAG"));
    } catch (error) {
      // @ts-ignore
      logger.error(error);
    } finally {
      setLoading(false);
    }
  };

  const deleteTagAction = async id => {
    setShowDeleteAlert(true);
    setselectedTagId(id);
  };

  if (loading) {
    return <PageLoader />;
  }
  return (
    <>
      <Header
        title="Tags"
        actionBlock={
          <Button
            onClick={() => setShowNewTagPane(true)}
            label="Add New Tag"
            icon="ri-add-line"
          />
        }
      />
      {tags.length ? (
        <>
          <SubHeader
            searchProps={{
              value: searchTerm,
              onChange: e => setSearchTerm(e.target.value),
              clear: () => setSearchTerm(""),
            }}
            deleteButtonProps={{
              onClick: () => setShowDeleteAlert(true),
              disabled: !selectedTagIds.length,
            }}
            paginationProps={{
              count: tags.length,
              pageNo: 1,
              pageSize: 15,
            }}
            toggleFilter={{
              value: "tag",
            }}
          />
          <TagTable
            selectedTagIds={selectedTagIds}
            setSelectedTagIds={setSelectedTagIds}
            deleteTagAction={deleteTagAction}
            tags={tags}
          />
        </>
      ) : (
        <EmptyState
          image={EmptyNotesListImage}
          title="Looks like you don't have any tags!"
          subtitle="Add your tags to send customized emails to them."
          primaryAction={() => setShowNewTagPane(true)}
          primaryActionLabel="Add New Tag"
        />
      )}
      <NewTagPane
        showPane={showNewTagPane}
        setShowPane={setShowNewTagPane}
        updateTagState={updateTagState}
      />
      {showDeleteAlert && (
        <DeleteAlert
          selectedTagIds={selectedTagIds}
          selectedTagId={selectedTagId}
          updateTagState={updateTagState}
          onClose={() => setShowDeleteAlert(false)}
        />
      )}
    </>
  );
};

export default Tags;
