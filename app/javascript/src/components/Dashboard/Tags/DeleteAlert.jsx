import React, { useState } from "react";
// @ts-ignore
import { Alert } from "neetoui";
import tagsApi from "apis/tags";

export default function DeleteAlert({
  onClose,
  selectedTagIds,
  selectedTagId,
  updateTagState,
}) {
  const [deleting, setDeleting] = useState(false);
  const handleDelete = async () => {
    try {
      setDeleting(true);
      if (selectedTagIds.length > 0) {
        await tagsApi.destroy({ ids: selectedTagIds });
      } else {
        await tagsApi.deleteTag(selectedTagId);
        updateTagState(selectedTagId, "DELETE_TAG");
      }
      onClose();
    } catch (error) {
      // @ts-ignore
      logger.error(error);
    } finally {
      setDeleting(false);
    }
  };
  return (
    <Alert
      isOpen
      size="small"
      autoHeight
      title={`Delete ${selectedTagIds.length > 0 ? selectedTagIds.length : 1}
        ${selectedTagIds.length > 1 ? "Tags" : "Tag"}`}
      submitButtonProps={{
        style: "danger",
        label: "Delete",
        loading: deleting,
        onClick: handleDelete,
      }}
      cancelButtonProps={{
        onClick: onClose,
      }}
      onClose={onClose}
    ></Alert>
  );
}
