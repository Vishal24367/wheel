import React from "react";
import PropTypes from "prop-types";

import TaskReducer from "reducers/task";

const TaskStateContext = React.createContext();
const TaskDispatchContext = React.createContext();
const initialState = { tasks: [] };

const TaskProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(TaskReducer, initialState);
  return (
    <TaskStateContext.Provider value={state}>
      <TaskDispatchContext.Provider value={dispatch}>
        {children}
      </TaskDispatchContext.Provider>
    </TaskStateContext.Provider>
  );
};

const useTaskState = () => {
  const context = React.useContext(TaskStateContext);
  if (context === undefined) {
    throw new Error("useTaskState must be used within a TaskProvider");
  }
  return context;
};

const useTaskDispatch = () => {
  const context = React.useContext(TaskDispatchContext);
  if (context === undefined) {
    throw new Error("useTaskDispatch must be used within a TaskProvider");
  }
  return context;
};

const useTask = () => {
  return [useTaskState(), useTaskDispatch()];
};

TaskProvider.propTypes = {
  children: PropTypes.node,
};

export { TaskProvider as TaskProvider, useTaskState, useTaskDispatch, useTask };
