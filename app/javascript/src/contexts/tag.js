import React from "react";
import PropTypes from "prop-types";

import TagReducer from "reducers/tag";

const TagStateContext = React.createContext();
const TagDispatchContext = React.createContext();
const initialState = { tags: [] };

const TagProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(TagReducer, initialState);
  return (
    <TagStateContext.Provider value={state}>
      <TagDispatchContext.Provider value={dispatch}>
        {children}
      </TagDispatchContext.Provider>
    </TagStateContext.Provider>
  );
};

const useTagState = () => {
  const context = React.useContext(TagStateContext);
  if (context === undefined) {
    throw new Error("useTagState must be used within a TagProvider");
  }
  return context;
};

const useTagDispatch = () => {
  const context = React.useContext(TagDispatchContext);
  if (context === undefined) {
    throw new Error("useTagDispatch must be used within a TagProvider");
  }
  return context;
};

const useTag = () => {
  return [useTagState(), useTagDispatch()];
};

TagProvider.propTypes = {
  children: PropTypes.node,
};

export { TagProvider as TagProvider, useTagState, useTagDispatch, useTag };
