import axios from "axios";

const fetch = () => axios.get("api/v1/notes");

const create = payload => axios.post("api/v1/notes", payload);

const destroy = payload => axios.post("api/v1/notes/bulk_delete", payload);

const deleteTask = id => axios.delete(`api/v1/notes/${id}`);

const moveTask = (id, payload) =>
  axios.put(`api/v1/notes/${id}/move_task`, payload);

const tasksApi = {
  fetch,
  create,
  destroy,
  deleteTask,
  moveTask,
};

export default tasksApi;
