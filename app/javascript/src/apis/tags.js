import axios from "axios";

const fetch = () => axios.get("api/v1/tags");

const create = payload => axios.post("api/v1/tags", payload);

const destroy = payload => axios.post("api/v1/tags/bulk_delete", payload);

const deleteTag = id => axios.delete(`api/v1/tags/${id}`);

const tagsApi = {
  fetch,
  create,
  destroy,
  deleteTag,
};

export default tagsApi;
