/* eslint-disable indent */
const taskReducer = (state, { type, payload }) => {
  switch (type) {
    case "ADD_TASK": {
      const index = state.tasks.findIndex(task => task.id === payload.id);

      if (index != -1)
        return {
          ...state,
        };
      else
        return {
          ...state,
          tasks: [...state.tasks, payload],
        };
    }
    case "DELETE_TASK": {
      const filteredTasks = state.tasks.filter(task => task.id !== payload.id);
      return {
        ...state,
        tasks: filteredTasks,
      };
    }
    case "UPDATE_TASK": {
      const index = state.tasks.findIndex(task => task.id === payload.id);
      const newArray = [...state.tasks];
      newArray[index].id = payload.id;
      newArray[index].title = payload.title;
      newArray[index].description = payload.description;
      newArray[index].state = payload.state;
      newArray[index].completed_date = payload.completed_date;
      newArray[index].due_date = payload.due_date;
      newArray[index].show_due_date = payload.show_due_date;
      newArray[index].tag_color = payload.tag_color;
      newArray[index].tag_name = payload.tag_name;
      newArray[index].created_at = payload.created_at;
      return {
        ...state,
        tasks: newArray,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${type}`);
    }
  }
};

export default taskReducer;
