/* eslint-disable indent */
const tagReducer = (state, { type, payload }) => {
  switch (type) {
    case "ADD_TAG": {
      const index = state.tags.findIndex(tag => tag.id === payload.id);

      if (index != -1)
        return {
          ...state,
        };
      else
        return {
          ...state,
          tags: [...state.tags, payload],
        };
    }
    case "DELETE_TAG": {
      const filteredTags = state.tags.filter(tag => tag.id !== payload.id);
      return {
        ...state,
        tags: filteredTags,
      };
    }
    case "UPDATE_TAG": {
      const index = state.tags.findIndex(tag => tag.id === payload.id);
      const newArray = [...state.tags];
      newArray[index].id = payload.id;
      newArray[index].name = payload.name;
      newArray[index].color = payload.color;
      return {
        ...state,
        tags: newArray,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${type}`);
    }
  }
};

export default tagReducer;
