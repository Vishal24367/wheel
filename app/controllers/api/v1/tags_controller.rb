# frozen_string_literal: true

class Api::V1::TagsController < Api::V1::BaseController
  before_action :load_tag, only: [:show, :destroy]

  def index
    tags = current_user.tags
    render_success_response({
      tags: array_serializer.new(tags, serializer: TagSerializer),
      }, "All #{current_user.name} tags are sucessfully fetched!")
  end

  def create
    @tag = current_user.tags.build(name: tag_params[:name], color: tag_params[:color][:value])
    if @tag.save
      render_success_response({
        tag: single_serializer.new(@tag, serializer: TagSerializer),
      }, "#{@tag.name.humanize} has been added to your tags!")
    else
      render json: { error: @tag.errors.full_messages.to_sentence }, status: :unprocessable_entity
    end
  end

  def destroy
    tag_name = @tag.name.humanize
    @tag.destroy
    render json: { message: "#{tag_name} has been deleted from your tags followed by tasks!" }
  end

  def bulk_delete
    tags = Tag.where(id: params[:ids], user: current_user)
    if tags.empty?
      render json: { error: "No users found with those IDs" }, status: :unprocessable_entity
    else
      tags_count = tags.size
      tags.destroy_all
      render json: { message: "#{tags_count} notes has been added deleted." }
    end
  end

  private

    def tag_params
      params.require(:tag).permit([ :name, color: [:value, :label]]).to_h
    end

    def load_tag
      @tag = Tag.find(params[:id])
      unless @tag.present?
        render json: { error: "Tag is not valid." }, status: :unprocessable_entity
      end
    end
end
