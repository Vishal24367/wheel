# frozen_string_literal: true

class Api::V1::NotesController < Api::V1::BaseController
  before_action :load_task, only: [:show, :destroy, :move_task]
  before_action :load_tag, only: [:create]

  def index
    notes = current_user.notes
    render_success_response({
      notes: array_serializer.new(notes, serializer: NoteSerializer),
      }, "All #{current_user.name} tasks are successfully fetched!")
  end

  def create
    @note = current_user.notes.build(title: note_params[:title], description: note_params[:description], tag: @tag, show_due_date: note_params[:showDueDateField], due_date: note_params[:dueDate])
    if @note.save
      render_success_response({
        note: single_serializer.new(@note, serializer: NoteSerializer),
      }, "#{@note.title.humanize} has been added to your tasks!")
    else
      render json: { error: @note.errors.full_messages.to_sentence }, status: :unprocessable_entity
    end
  end

  def move_task
    new_state = params[:state].downcase.strip
    begin
      ActiveRecord::Base.transaction do
        @note.mark_in_progress! if new_state == "in_progress"
        @note.mark_on_hold! if new_state == "on_hold"
        @note.mark_completed! if new_state == "completed"
        @note.mark_closed! if new_state == "closed"
        render_success_response({
          note: single_serializer.new(@note, serializer: NoteSerializer),
        }, "#{@note.title.humanize} has been successfully updated!")
      end
    rescue StandardError => e
      ExceptionLogger.log_exception(e, info: `Some error occured while updating the #{@note.title.humanize} task`)
      render json: { errors: [{ message: e.message }] }, status: :unprocessable_entity
    end
  end

  def destroy
    title = @note.title.humanize
    @note.destroy
    render json: { message: "#{title} has been deleted from your tasks!" }
  end

  def bulk_delete
    notes = Note.where(id: params[:ids], user: current_user)
    if notes.empty?
      render json: { error: "No task found with those IDs" }, status: :unprocessable_entity
    else
      notes_count = notes.size
      notes.destroy_all
      render json: { message: "#{notes_count} tasks has been deleted." }
    end
  end

  private

    def note_params
      params.require(:note).permit([:title, :description, :dueDate, :showDueDateField, :state, tag: [:value, :label]]).to_h
    end

    def load_task
      @note = Note.find(params[:id])
      unless @note.present?
        render json: { error: "Task is not valid." }, status: :unprocessable_entity
      end
    end

    def load_tag
      @tag = Tag.find_by(id: note_params[:tag][:value]) rescue nil
      unless @tag.present?
        render json: { error: "Tag is not valid." }, status: :unprocessable_entity
      end
    end
end
