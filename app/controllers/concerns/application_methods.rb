# frozen_string_literal: true

module ApplicationMethods
  extend ActiveSupport::Concern

  included do
    around_action :handle_exceptions
  end

  private

    def handle_exceptions
      begin
        yield
      rescue ActiveRecord::RecordNotFound => e
        @status = 404
      rescue ArgumentError => e
        @status = 400
      rescue ActiveRecord::RecordInvalid => e
        render_unprocessable_entity_response(e.record) && return
      rescue StandardError => e
        @status = 500
      end

      detail = { detail: e&.message }
      detail.merge!(trace: e&.backtrace) if Rails.env.development?

      return if e.class == NilClass
      json_response({ success: false, message: e.class.to_s, errors: [detail] }, @status)
    end

    def render_unprocessable_entity_response(resource)
      json_response({ success: false, message: 'Validation failed', errors: ValidationErrorsSerializer.new(resource).serialize }, 422)
    end

    def render_unprocessable_entity(message)
      json_response({ success: false, errors: message }, 422) && return
    end

    def render_success_response(resources = {}, message = '', meta = {}, status = 200)
      json_response({ success: true, message: message, data: resources, meta: meta }, status)
    end

    def json_response(options = {}, status = 500)
      render json: JsonResponse.new(options), status: status
    end

    def render_unauthorized_response
      json_response({ success: false, message: 'You are not authorized.' }, 401)
    end

    def array_serializer
      ActiveModel::Serializer::CollectionSerializer
    end

    def single_serializer
      ActiveModelSerializers::SerializableResource
    end
end