# frozen_string_literal: true

class Note < ApplicationRecord
  include AASM
  belongs_to :user
  belongs_to :tag

  validates :title, :description, :tag, :user, presence: true
  validates :title, uniqueness: true

  validate :require_due_date_if_checked

  aasm column: 'state' do
    state :newly_created, initial: true
    state :in_progress
    state :on_hold
    state :completed
    state :closed

    event :mark_in_progress do
      transitions from: [:newly_created, :on_hold], to: :in_progress
    end

    event :mark_on_hold do
      transitions from: :in_progress, to: :on_hold
    end

    event :mark_completed do
      transitions from: :in_progress, to: :completed
    end

    event :mark_closed do
      transitions from: [:in_progress, :newly_created, :on_hold], to: :closed
    end
  end

  private
    def require_due_date_if_checked
      errors.add(:base, "You must provide at due date to this task") if show_due_date && due_date == nil
    end
end
