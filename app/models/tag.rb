# frozen_string_literal: true

class Tag < ApplicationRecord
  belongs_to :user

  has_many :notes, dependent: :delete_all

  validates :name, :color, presence: true
  validates :name, uniqueness: true

  enum color: [:blue, :green, :red, :yellow, :grey]
end
